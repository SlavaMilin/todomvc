> Cypress + React + Zeit + GitLab CI = 🎉

Example React application tested at different stages
using [Cypress.io](https://www.cypress.io/) tool.

The end to end tests are running:

- [x] local development using TDD
- [x] before pushing code to remote server (Git `pre-push` hook)
- [x] on CI against a local server
- [x] on CI against an immutable deploy to Zeit cloud
- [x] on CI against production URL after switching Zeit alias
